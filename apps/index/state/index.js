module.exports = {
  page: {
    title: 'Repo manager',
    header: 'Repo manager',
    content: 'Hello',
    lists: {
      users: [{
        ui: 'large orange',
        icon: 'user',
        label: 'kmandrup',
        desc: 'I DREAM in CODE'
      },
      {
        ui: 'large blue',
        icon: 'user',
        label: 'isaura',
        desc: 'I LOVE web DESIGN'
      }]
    }
  }
}
