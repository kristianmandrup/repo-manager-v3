Components
==========

Currently use Semantic. The slush generator should have an option to pre-generate a set of taglibs for a given UI library such as Semantic.

The taglibs and tags auto-included:

```sh
/button
  /act-button
  /link-button
  /animated-button
/feed
  /article-feed
/item
  /link-item
  /image-item
  /icon-item
/list
  /icon-list
  /image-list
  /txt-list
/menu
  /main-menu
  /session-menu
  /side-menu
/page  
  /page-title
/search
  /search-box
/side
  /side-bar
  /side-menu
```
