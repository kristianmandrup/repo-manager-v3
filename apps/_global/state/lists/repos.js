module.exports = [{
  size: 'large',
  color: 'orange',
  icon: 'github',
  label: 'markoa',
  desc: 'Koa Marko server'
},
{
  size: 'large',
  color: 'blue',
  icon: 'github',
  label: 'marko-taglib',
  desc: 'Taglibs for marko'
}];
