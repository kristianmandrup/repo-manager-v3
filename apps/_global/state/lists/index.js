module.exports = {
  repos: require('./repos'),
  teams: require('./teams'),
  users: require('./users')
}
